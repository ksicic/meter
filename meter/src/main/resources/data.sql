INSERT INTO METER (code) values ('Code12310');
INSERT INTO METER (code) values ('Code21316');
INSERT INTO METER (code) values ('Code12312');
INSERT INTO METER (code) values ('Code12324');

INSERT INTO ADDRESS (street, house_number, city) values ('Riječka ulica', 6, 'Zagreb');
INSERT INTO ADDRESS (street, house_number, city) values ('Ilica', 57, 'Zagreb');
INSERT INTO ADDRESS (street, house_number, city) values ('Dubrovačka ulica', 22, 'Zagreb');
INSERT INTO ADDRESS (street, house_number, city) values ('Put murvice', 43, 'Zadar');

INSERT INTO CLIENT (first_name, last_name, address_id, meter_id) values ('Petar', 'Perić', 1, 1);
INSERT INTO CLIENT (first_name, last_name, address_id, meter_id) values ('Mirta', 'Mirić', 2, 2);
INSERT INTO CLIENT (first_name, last_name, address_id, meter_id) values ('Bero', 'Berić', 3, 3);
INSERT INTO CLIENT (first_name, last_name, address_id, meter_id) values ('Iva', 'Ivić', 4, 4);

INSERT INTO METER_READING (meter_reading_date, consumption, meter_id) values (date '2019-07-01', 45, 1);
INSERT INTO METER_READING (meter_reading_date, consumption, meter_id) values (date '2019-05-01', 65, 1);
INSERT INTO METER_READING (meter_reading_date, consumption, meter_id) values (date '2019-01-01', 76, 1);
INSERT INTO METER_READING (meter_reading_date, consumption, meter_id) values (date '2019-06-01', 821, 1);
INSERT INTO METER_READING (meter_reading_date, consumption, meter_id) values (date '2019-04-01', 22, 1);
INSERT INTO METER_READING (meter_reading_date, consumption, meter_id) values (date '2019-03-01', 84, 1);
INSERT INTO METER_READING (meter_reading_date, consumption, meter_id) values (date '2019-02-01', 5, 1);
INSERT INTO METER_READING (meter_reading_date, consumption, meter_id) values (date '2019-12-31', 120, 2);
INSERT INTO METER_READING (meter_reading_date, consumption, meter_id) values (date '2019-05-01', 22, 3);
INSERT INTO METER_READING (meter_reading_date, consumption, meter_id) values (date '2018-01-01', 33, 3);