package com.example.meter.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;

import com.example.meter.dao.MeterDao;
import com.example.meter.model.Meter;
import com.example.meter.model.MeterReading;
import com.example.meter.validation.MeterReadingExistException;

@Service
public class MeterService {

	@Autowired
	private MeterDao meterDao;

	public Map<String, String> getAggregateMeterReadingsForYear(Long clientId, int year) {
		Map<String, String> map = new LinkedHashMap<>();
		BigDecimal sum = BigDecimal.ZERO;

		List<MeterReading> meterReadingList = getMeterReadingsForYearByClientId(clientId, year);

		for (MeterReading item : meterReadingList) {
			sum = sum.add(item.getConsumption());
		}

		map.put("YEAR", String.valueOf(year));
		map.put("TOTAL", String.valueOf(sum));

		return map;
	}

	public Map<String, String> getMeterReadingsForYear(Long clientId, int year) {
		Map<String, String> map = new LinkedHashMap<>();
		map.put("YEAR", String.valueOf(year));

		List<MeterReading> meterReadingList = getMeterReadingsForYearByClientId(clientId, year);

		for (MeterReading item : meterReadingList) {
			map.put(item.getMeterReadingDate().getMonth().name(), item.getConsumption().toString());
		}

		return map;
	}

	public Map<String, String> getMeterReadingForYearAndMonth(Long clientId, int year, int month) {
		Map<String, String> map = new LinkedHashMap<>();
		map.put("YEAR", String.valueOf(year));

		List<MeterReading> meterReadingList = getMeterReadingsForYearByClientId(clientId, year);

		Optional<MeterReading> matchingObject = meterReadingList.stream().filter(
				e -> e.getMeterReadingDate().getYear() == year && e.getMeterReadingDate().getMonthValue() == month)
				.findFirst();

		if (matchingObject.isPresent()) {
			MeterReading meterReading = matchingObject.get();
			map.put(meterReading.getMeterReadingDate().getMonth().name(), meterReading.getConsumption().toString());
		}

		return map;
	}

	@Transactional
	public void addMeterReadingForMonthAndYear(Long clientId, int year, int month, BigDecimal consumption)
			throws Exception {
		Meter meter = getMeterByClientId(clientId);

		Optional<MeterReading> matchingObject = meter.getMeterReadingSet().stream()
				.filter(e -> e.getMeterReadingDate().getYear() == year
						&& e.getMeterReadingDate().getMonth().getValue() == month)
				.findFirst();

		if (matchingObject.isPresent()) {
			throw new MeterReadingExistException(
					"Meter reading for the specified client, month and year already exist!");
		}

		MeterReading meterReading = new MeterReading();
		LocalDate meterReadingDate = LocalDate.of(year, month, 1);
		meterReading.setMeterReadingDate(meterReadingDate);
		meterReading.setConsumption(consumption);
		meter.addMeterReading(meterReading);

		saveMeter(meter);
	}

	@Transactional
	public List<MeterReading> getMeterReadingsForYearByClientId(Long clientId, int year) {
		return meterDao.getMeterReadingsForYearByClientId(clientId, year);
	}

	@Transactional
	public Meter getMeterByClientId(Long clientId) {
		return meterDao.getMeterByClientId(clientId);
	}

	@Transactional
	public void saveMeter(Meter meter) {
		meterDao.saveMeter(meter);
	}

}
