package com.example.meter.controller;

import java.math.BigDecimal;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.meter.service.MeterService;

@RestController
@RequestMapping("/meter")
public class MeterController {

	@Autowired
	private MeterService meterService;

	@GetMapping("/getAggregateMeterReadingsForYear")
	public Map<String, String> getAggregateMeterReadingsForYear(@RequestParam Long clientId, @RequestParam int year) {
		return meterService.getAggregateMeterReadingsForYear(clientId, year);
	}

	@GetMapping("/getMeterReadingsForYear")
	public Map<String, String> getMeterReadingsForYear(@RequestParam Long clientId, @RequestParam int year) {
		return meterService.getMeterReadingsForYear(clientId, year);
	}

	@GetMapping("/getMeterReadingForYearAndMonth")
	public Map<String, String> getMeterReadingForYearAndMonth(@RequestParam Long clientId, @RequestParam int year,
			@RequestParam int month) {
		return meterService.getMeterReadingForYearAndMonth(clientId, year, month);
	}

	@GetMapping("/addMeterReadingForMonthAndYear")
	public void addMeterReadingForMonthAndYear(@RequestParam Long clientId, @RequestParam int year,
			@RequestParam int month, @RequestParam BigDecimal consumption) throws Exception  {
		meterService.addMeterReadingForMonthAndYear(clientId, year, month, consumption);
	}

}
