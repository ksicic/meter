package com.example.meter.validation;

public class MeterReadingExistException extends Exception {

	private static final long serialVersionUID = 4860316949704885124L;

	public MeterReadingExistException(String errorMessage) {
		super(errorMessage);
	}

}
