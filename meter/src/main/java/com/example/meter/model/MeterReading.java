package com.example.meter.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
@Table(name = "METER_READING")
public class MeterReading {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "meter_id", nullable = false)
	private Meter meter;

	@Column(name = "METER_READING_DATE")
	private LocalDate meterReadingDate;

	@Column(name = "CONSUMPTION")
	private BigDecimal consumption;

}
