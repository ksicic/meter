package com.example.meter.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
@Table(name = "METER")
public class Meter {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "CODE")
	private String code;

	@OneToOne(mappedBy = "meter")
	private Client client;

	@OneToMany(mappedBy = "meter", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<MeterReading> meterReadingSet = new HashSet<>();

	public void addMeterReading(MeterReading meterReading) {
		this.meterReadingSet.add(meterReading);
		meterReading.setMeter(this);
	}

}
