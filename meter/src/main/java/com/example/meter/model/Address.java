package com.example.meter.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
@Table(name = "ADDRESS")
public class Address {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@OneToOne(mappedBy = "address")
	private Client client;

	@Column(name = "STREET")
	private String street;

	@Column(name = "HOUSE_NUMBER")
	private Long houseNumber;

	@Column(name = "CITY")
	private String city;

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof Address)) {
			return false;
		}
		Address address = (Address) o;
		return street.equals(address.street) && city.equals(address.city)
				&& Long.compare(houseNumber, address.houseNumber) == 0;
	}

}
