package com.example.meter.dao;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.meter.model.Meter;
import com.example.meter.model.MeterReading;

@Repository
public class MeterDao {

	@Autowired
	private EntityManager entityManager;

	public List<MeterReading> getMeterReadingsForYearByClientId(Long clientId, int year) {
		LocalDate startDate = LocalDate.of(year, Month.JANUARY, 1);
		LocalDate endDate = LocalDate.of(year, Month.DECEMBER, 31);

		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<MeterReading> criteria = builder.createQuery(MeterReading.class);

		Root<MeterReading> variableRoot = criteria.from(MeterReading.class);
		criteria.select(variableRoot).orderBy(builder.asc(variableRoot.get("meterReadingDate"))).where(
				builder.equal(variableRoot.get("meter").get("client").get("id"), clientId),
				builder.between(variableRoot.get("meterReadingDate"), startDate, endDate));

		return entityManager.createQuery(criteria).getResultList();
	}

	public Meter getMeterByClientId(Long clientId) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Meter> criteria = builder.createQuery(Meter.class);

		Root<Meter> variableRoot = criteria.from(Meter.class);
		criteria.select(variableRoot).where(builder.equal(variableRoot.get("client").get("id"), clientId));

		return entityManager.createQuery(criteria).getSingleResult();
	}

	public void saveMeter(Meter meter) {
		entityManager.merge(meter);
		entityManager.flush();
	}

}
