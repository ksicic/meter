package com.example.meter;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import com.example.meter.model.Meter;
import com.example.meter.model.MeterReading;
import com.example.meter.service.MeterService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class MeterApplicationTests {

	@LocalServerPort
	private int port;

	@Autowired
	private MeterService meterService;

	private UriComponentsBuilder builder;

	private TestRestTemplate restTemplate = new TestRestTemplate();

	@Test
	void getAggregateMeterReadingsForYearTest() {
		builder = UriComponentsBuilder.fromHttpUrl(createUrlWithPort("/getAggregateMeterReadingsForYear"));
		builder.queryParam("clientId", 1);
		builder.queryParam("year", 2017);

		ResponseEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, null, Map.class);

		assertFalse(response.getBody() == null);
	}

	@Test
	void getMeterReadingsForYearTest() {
		builder = UriComponentsBuilder.fromHttpUrl(createUrlWithPort("/getMeterReadingsForYear"));
		builder.queryParam("clientId", 1);
		builder.queryParam("year", 2017);

		ResponseEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, null, Map.class);

		assertFalse(response.getBody() == null);
	}

	@Test
	void getMeterReadingForYearAndMonth() {
		builder = UriComponentsBuilder.fromHttpUrl(createUrlWithPort("/getMeterReadingForYearAndMonth"));
		builder.queryParam("clientId", 1);
		builder.queryParam("month", 5);
		builder.queryParam("year", 2017);

		ResponseEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, null, Map.class);

		assertFalse(response.getBody() == null);
	}

	@Test
	void addMeterReadingForMonthAndYear() {
		builder = UriComponentsBuilder.fromHttpUrl(createUrlWithPort("/addMeterReadingForMonthAndYear"));
		builder.queryParam("clientId", 2);
		builder.queryParam("month", 5);
		builder.queryParam("year", 2017);
		builder.queryParam("consumption", 56.54);

		ResponseEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, null, Void.class);

		Meter meter = meterService.getMeterByClientId(2L);
		Optional<MeterReading> matchingObject = meter.getMeterReadingSet().stream()
				.filter(e -> e.getMeterReadingDate().getYear() == 2017
						&& e.getMeterReadingDate().getMonth().getValue() == 5
						&& e.getConsumption().equals(BigDecimal.valueOf(56.54)))
				.findFirst();

		assertTrue(response.getStatusCode().equals(HttpStatus.OK) && matchingObject.isPresent());
	}

	private String createUrlWithPort(String url) {
		return "http://localhost:" + port + "/meter" + url;
	}

}
