# METER-API APPLICATION #

Meter-api is Spring Boot application that is used for reach and insert meter readings of consumption in house


### Setup ###

To run this project, install it with - maven clean install

After that, in target folder there will be war package test-api.war

Install test-api.war on service (e.g. Tomcat)

Application will be started on port 8080

Initial data are stored in memory H2 database, they are initialized in file src/main/resources/data.sql

### Service examples ###

Application contains 4 services:




- getAggregateMeterReadingsForYear (params: clientId, year)


example: http://localhost:8080/meter-api/meter/getAggregateMeterReadingsForYear?clientId=1&year=2019


- getMeterReadingsForYear(params: clientId, year)


example: http://localhost:8080/meter-api/meter/getMeterReadingsForYear?clientId=1&year=2019


- getMeterReadingForYearAndMonth(params: clientId, year, month)


example: http://localhost:8080/meter-api/meter/getMeterReadingForYearAndMonth?clientId=1&year=2019&month=2


- addMeterReadingForMonthAndYear (params: clientId, year, month, consumption)


example: http://localhost:8080/meter-api/meter/addMeterReadingForMonthAndYear?clientId=1&year=2017&month=2&consumption=45.44


### Test class ###

Class MeterApplicationTests contains integration tests for services

